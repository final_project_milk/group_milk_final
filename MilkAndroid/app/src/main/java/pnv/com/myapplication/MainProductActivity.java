package pnv.com.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/26/2015.
 */
public class MainProductActivity extends Activity {

    private ProgressDialog pDialog;
    ListView lv;
    MyArrayAdapterProduct adapter = null;
    List<Product> rowProduct;
    JSONParser jParser = new JSONParser();
    private static String url_all_products = "http://192.168.56.1:8084/MilkGroupAnd/api/product";


    JSONArray productsArr = null;
    int posSt = -1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_product);

        rowProduct  = new ArrayList<Product>();
        lv =(ListView) findViewById(R.id.list);
        new LoadAllProducts().execute();

    }

    class LoadAllProducts extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainProductActivity.this);
            pDialog.setMessage("Loading products. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            productsArr = jParser.makeHttpRequestArray(url_all_products, "GET", params);

            JSONObject product = null;
            Product prodictobj = null;
            List<Product> test_lst = new ArrayList<Product>();

            for (int i = 0; i < productsArr.length(); i++) {
                try {
                    product = productsArr.getJSONObject(i);
                    String imgpre = product.getString("image");
                    String img = "http://192.168.56.1:8084/MilkGroupAnd/resources/" + imgpre;
                    String idProduct = product.getString("idProduct");
                    String nameProduct  = product.getString("name");
                    String price = product.getString("price");
                    String quantity = product.getString("quantity");
                    String productionDate = product.getString("productionDate");
                    String expireDate = product.getString("expireDate");

                    Product std = new Product(img,idProduct,nameProduct ,price, quantity, productionDate, expireDate );
                    rowProduct.add(std);

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
//            for (int i=0; i< test_lst.size(); i++) {
//                Product studentobj1 = test_lst.get(i);
//                Log.i("Product info :", studentobj1.getId() +" "+studentobj1.getNameStudent() );
//            }
            Log.i("product:", rowProduct.toString());
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            adapter = new MyArrayAdapterProduct(MainProductActivity.this ,R.layout.row_product_detail, rowProduct);
            lv.setAdapter(adapter);

//            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                    Toast.makeText(MainActivity.this, "hello", Toast.LENGTH_SHORT).show();
//                    Toast toast = Toast.makeText(getApplicationContext(),
//                            "Item " + (position + 1) + ": " + rowProduct.get(position),
//                            Toast.LENGTH_SHORT);
//                    toast.show();
//                    Intent myIntent = new Intent(MainActivity.this, Edit_Product.class);
//
//                    Bundle bundle = new Bundle();
//                    Product pt =  rowProduct.get(position);
//
//                    bundle.putString("id", pt.getIdProduct());
//                    bundle.putString("name", pt.getName());
//                    bundle.putString("price", pt.getPrice());
//                    bundle.putString("quantity", pt.getQuantity());
//                    bundle.putString("productionDate", pt.getProductionDate());
//                    bundle.putString("expireDate", pt.getExpireDate());
//
//                    myIntent.putExtra("PackageGiveData", bundle);
//                    startActivity(myIntent);
//                }
//            });

            registerForContextMenu(lv);
            lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(getActivity(), " "+ position, Toast.LENGTH_SHORT).show();
                    posSt = position;
                    // Toast.makeText(getActivity(), " "+ posSt, Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            pDialog.dismiss();
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub

        menu.setHeaderIcon(R.drawable.ic_lanuncher);
        menu.setHeaderTitle("Edit Item");

        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu_product, menu);

    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case R.id.back:
                Intent intent = new Intent(this,
                        MainActivity.class);
                startActivity(intent);
                break;
            case R.id.edit:
                Intent myIntent = new Intent(MainProductActivity.this, Edit_Product.class);

                Bundle bundle = new Bundle();
                Product pt =  rowProduct.get(posSt);

                // bundle.putInt("position", posSt);
                bundle.putString("id", pt.getIdProduct());
                bundle.putString("name", pt.getName());
                bundle.putString("price", pt.getPrice());
                bundle.putString("quantity", pt.getQuantity());
                bundle.putString("productionDate", pt.getProductionDate());
                bundle.putString("expireDate", pt.getExpireDate());
                bundle.putString("imageProduct", pt.getImage());

                myIntent.putExtra("PackageGiveData", bundle);
                startActivity(myIntent);
                break;
            case R.id.delete:
                idDelete = rowProduct.get(posSt).getIdProduct().toString();
                Toast.makeText(MainProductActivity.this, " " + posSt, Toast.LENGTH_SHORT).show();
                new DeleteProduct().execute();
                break;

            case R.id.add:
                Intent intentadd = new Intent(this,
                        Add_Product.class);
                startActivity(intentadd);
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }


    private String idDelete;
    JSONParser jsonParser = new JSONParser();
    private static final String url_delete_product = "http://192.168.56.1:8084/MilkGroupAnd/api/product/delete/";

    class DeleteProduct extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainProductActivity.this);
            pDialog.setMessage("Deleting Product...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Deleting product
         * */
        protected String doInBackground(String... args) {


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("pid", idDelete));

            // getting product details by making HTTP request
            JSONArray json = jsonParser.makeHttpRequestArray(
                    url_delete_product + "/" + idDelete, "GET", params);

            // check your log for json response
            Log.d("Delete product", json.toString());

            Intent i = new Intent(MainProductActivity.this, MainProductActivity.class);
            startActivity(i);

            // closing this screen
            // finish();
            return null;

        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product deleted
            pDialog.dismiss();

        }

    }
}
