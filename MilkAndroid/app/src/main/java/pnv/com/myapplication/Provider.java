package pnv.com.myapplication;

/**
 * Created by linhnn on 5/11/2015.
 */
public class Provider {
    private  String idProvider;
    private  String nameOfCountry;

    public String getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(String idProvider) {
        this.idProvider = idProvider;
    }

    public String getNameOfCountry() {
        return nameOfCountry;
    }

    public void setNameOfCountry(String nameOfCountry) {
        this.nameOfCountry = nameOfCountry;
    }
}
