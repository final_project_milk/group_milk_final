package pnv.com.myapplication;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/24/2015.
 */
public class RightTypeProductFragmentActivity extends Fragment {

    Button btnAdd, btnEdit, btnDelete, btnBack;
    TextView etIdTypeProduct;
    EditText etTypeProduct;

    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    private static final String url_delete_type_product = "http://192.168.56.1:8084/MilkGroupAnd/api/typeproduct/delete";



    private String idDelete;
    public void change(String id, String type){
        etIdTypeProduct.setText(id);
        etTypeProduct.setText(type);

        idDelete= etIdTypeProduct.getText().toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_right_type_product, container, false);

        etIdTypeProduct = (TextView) v.findViewById(R.id.etIdTypeProductRight);
        etTypeProduct = (EditText) v.findViewById(R.id.etTypeProductRight);


        btnBack = (Button) v.findViewById(R.id.btnBack);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity() , MainActivity.class);
                startActivity(intent);
                Toast.makeText(getActivity(), "Execute", Toast.LENGTH_SHORT).show();
            }
        });


        btnAdd = (Button) v.findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(getActivity(), "click add",
                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(),
                        Add_Type_Product.class);
                startActivity(intent);
            }
        });

        btnEdit = (Button) v.findViewById(R.id.btnEdit);

        btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // Toast.makeText(getActivity(), "click Edit" + etId.getText().toString() +"Name:  "+etName.getText().toString() + "Class" + etClass.getText().toString(),
                //  Toast.LENGTH_SHORT).show();

                Intent myIntent=new Intent(getActivity(), Edit_Type_Product.class);

                Bundle bundle=new Bundle();
//                //Student items = rowStudent.get(arg2);
                bundle.putString("idTypeProduct", etIdTypeProduct.getText().toString());
                bundle.putString("typeProduct", etTypeProduct.getText().toString());

                myIntent.putExtra("PackageGiveData", bundle);

                startActivity(myIntent);
            }
        });



        btnDelete = (Button) v.findViewById(R.id.btnDelete);

        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DeleteTypeProduct().execute();
            }
        });


        return v;
    }




    class DeleteTypeProduct extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Deleting Type Product...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("pid", idDelete));

            JSONArray json = jsonParser.makeHttpRequestArray(
                    url_delete_type_product + "/" + idDelete, "GET", params);

            Log.d("Delete Type Product", json.toString());

            Intent i = new Intent(getActivity(), MainTypeProductActivity.class);
            startActivity(i);

            // closing this screen
            // finish();
            return null;

        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product deleted
            pDialog.dismiss();

        }

    }
}
