package pnv.com.myapplication;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Button btnProduct, btnProvider, btnTypeProduct;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        btnProduct = (Button) findViewById(R.id.btnSTUDENT);

        btnProduct.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getApplicationContext(), "click get product",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),
						MainProductActivity.class);
				startActivity(intent);
			}
		});

        btnProvider = (Button) findViewById(R.id.btnPROVIDER);

        btnProvider.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "click get product",
                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(),
                        MainProviderActivity.class);
                startActivity(intent);
            }
        });

        btnTypeProduct = (Button) findViewById(R.id.btnTYPEPRODUCT);

        btnTypeProduct.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "click get product",
                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(),
                        MainTypeProductActivity.class);
                startActivity(intent);
            }
        });


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
