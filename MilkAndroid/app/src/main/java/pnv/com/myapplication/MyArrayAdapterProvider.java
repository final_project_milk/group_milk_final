package pnv.com.myapplication;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by linhnn on 5/20/2015.
 */
public class MyArrayAdapterProvider extends ArrayAdapter<Provider> {

    Activity context;
    ArrayList<Provider> myArray= null;
    int layoutId;
   // ArrayList<HashMap<String, String>> myArray;

    public MyArrayAdapterProvider(Activity context, int resource, ArrayList<Provider> arr) {
        super(context, resource, arr);
        this.context=context;
        this.layoutId= resource;
        this.myArray = arr;
    }


    public View getView(int position, View convertView,
                        ViewGroup parent) {

        LayoutInflater inflater=
                context.getLayoutInflater();
        convertView=inflater.inflate(layoutId, null);

        if(myArray.size()>0 && position>=0)
        {

            TextView etId =(TextView)
                    convertView.findViewById(R.id.etId);
            TextView etName=(TextView)
                    convertView.findViewById(R.id.etName);


            Provider provider =myArray.get(position);

            etId.setText(provider.getIdProvider());
            etName.setText(provider.getNameOfCountry());



//            ImageView imgitem=(ImageView)
//                    convertView.findViewById(R.id.icon);
//
//            imgitem.setImageResource(student.getImage());

        }

        return convertView;
    }
}
