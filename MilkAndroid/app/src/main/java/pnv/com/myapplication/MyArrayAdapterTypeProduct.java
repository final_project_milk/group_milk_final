package pnv.com.myapplication;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/24/2015.
 */
public class MyArrayAdapterTypeProduct  extends ArrayAdapter<TypeProduct> {

    Activity context;
    ArrayList<TypeProduct> myArray= null;
    int layoutId;

    public MyArrayAdapterTypeProduct(Activity context, int resource, ArrayList<TypeProduct> arr) {
        super(context, resource, arr);
        this.context=context;
        this.layoutId= resource;
        this.myArray = arr;
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {

        LayoutInflater inflater=
                context.getLayoutInflater();
        convertView=inflater.inflate(layoutId, null);

        if(myArray.size()>0 && position>=0)
        {

            TextView etId =(TextView)
                    convertView.findViewById(R.id.etIdTypeProduct);
            TextView etType=(TextView)
                    convertView.findViewById(R.id.etTypeProduct);


            TypeProduct typeProduct =myArray.get(position);

            etId.setText(typeProduct.getIdTypeProduct());
            etType.setText(typeProduct.getTypeProduct());

        }

        return convertView;
    }

}
