package pnv.com.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/20/2015.
 */
public class Edit_Provider extends Activity {
    Button btnEdit, btnBack;



    TextView etId;
    EditText etName ;
    // Progress Dialog
    private ProgressDialog pDialog;

    // JSON parser class
    JSONParser jsonParser = new JSONParser();


    // url to update product
    private static final String url_update_product = "http://192.168.56.1:8084/MilkGroupAnd/api/provider/edit";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_provider);

        etId = (TextView) findViewById(R.id.etIdRight);
        etName = (EditText) findViewById(R.id.etNameRight);


        Intent callerIntent = getIntent();
        Bundle packageFromCaller = callerIntent.getBundleExtra("PackageGiveData");
        String IdStu = packageFromCaller.getString("idProvider");
        String name =packageFromCaller.getString("nameOfCountry");


        etId.setText(IdStu);
        etName.setText(name);



        btnEdit = (Button) findViewById(R.id.btnEditStudent);

        // button click event
        btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                new EditStudentDetails().execute();
            }
        });

        btnBack = (Button) findViewById(R.id.btnBack);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Edit_Provider.this , MainProviderActivity.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Execute", Toast.LENGTH_SHORT).show();
            }
        });


    }

    class EditStudentDetails extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Edit_Provider.this);
            pDialog.setMessage("Edit Provider ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Saving product
         * */
        protected String doInBackground(String... args) {

            // getting updated data from EditTexts
            String id = etId.getText().toString();
            String NameProvider = etName.getText().toString();


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("idProvider", id));
            params.add(new BasicNameValuePair("nameOfCountry", NameProvider));



            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONArray json = jsonParser.makeHttpRequestArray(url_update_product + "/" + id + "/" + NameProvider ,
                    "GET", params);

            Intent i = new Intent(getApplicationContext(), MainProviderActivity.class);
            startActivity(i);

            // closing this screen
            finish();
            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            pDialog.dismiss();
        }
    }

}
