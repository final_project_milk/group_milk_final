package pnv.com.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/25/2015.
 */
public class Edit_Product extends Activity {
Button btnAdd, btnEdit, btnDelete, btnBack;
TextView tvIdProduct;
EditText etName, etPrice, etQuantity, etProductionDate, etExpireDate;
    ImageView imageProduct;

    private ProgressDialog pDialog;
    // JSON parser class
    JSONParser jsonParser = new JSONParser();
    // url to update product
    private static final String url_update_product = "http://192.168.56.1:8084/MilkGroupAnd/api/product/edit";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_product);

        imageProduct = (ImageView) findViewById(R.id.thumbnail);
        tvIdProduct = (TextView) findViewById(R.id.etIdProduct);

        etName=(EditText) findViewById(R.id.etName);
        etPrice=(EditText) findViewById(R.id.etPrice);
        etQuantity=(EditText) findViewById(R.id.etQuantity);
        etProductionDate = (EditText) findViewById(R.id.etProductionDate);
        etExpireDate = (EditText) findViewById(R.id.etExpireDate);

        Intent callerIntent = getIntent();
        Bundle packageFromCaller = callerIntent.getBundleExtra("PackageGiveData");
        String idPro = packageFromCaller.getString("id");
        String name =packageFromCaller.getString("name");
        String price = packageFromCaller.getString("price");
        String quantity =packageFromCaller.getString("quantity");
        String productionDate = packageFromCaller.getString("productionDate");
        String expireDate =packageFromCaller.getString("expireDate");

        String imageGet= packageFromCaller.getString("imageProduct");
       // int position = packageFromCaller.getInt("position");



        tvIdProduct.setText(idPro);
        etName.setText(name);
        etPrice.setText(price);
        etQuantity.setText(quantity);
        etProductionDate.setText(productionDate);
        etExpireDate.setText(expireDate);
        new DownloadImageTask(imageProduct).execute(imageGet);


        btnEdit = (Button) findViewById(R.id.btEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                new EditProduct().execute();
            }
        });


        btnBack = (Button) findViewById(R.id.btBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Edit_Product.this, MainProductActivity.class);
                startActivity(intent);
            }
        });
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    class EditProduct extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Edit_Product.this);
            pDialog.setMessage("Edit Provider ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Saving product
         * */
        protected String doInBackground(String... args) {

            // getting updated data from EditTexts
            String id = tvIdProduct.getText().toString();
            String NameProduct = etName.getText().toString();
            String price = etPrice.getText().toString();
            String quantity = etQuantity.getText().toString();


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("idProvider", id));
            params.add(new BasicNameValuePair("nameOfCountry", NameProduct));



            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONArray json = jsonParser.makeHttpRequestArray(url_update_product + "/" + id + "/" + NameProduct + "/"+price+"/"+quantity ,
                    "GET", params);

            Intent i = new Intent(Edit_Product.this, MainProductActivity.class);
            startActivity(i);

            // closing this screen
            finish();
            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            pDialog.dismiss();
        }
    }

}
