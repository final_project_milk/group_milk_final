package pnv.com.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/24/2015.
 */
public class Edit_Type_Product extends Activity {
    Button btnEdit, btnBack;

    TextView etIdType;
    EditText etTypeProduct ;

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_update_type_product = "http://192.168.56.1:8084/MilkGroupAnd/api/typeproduct/edit";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_type_product);

        etIdType = (TextView) findViewById(R.id.etIdTypeProduct);
        etTypeProduct = (EditText) findViewById(R.id.etTypeProduct);


        Intent callerIntent = getIntent();
        Bundle packageFromCaller = callerIntent.getBundleExtra("PackageGiveData");
        String IdStu = packageFromCaller.getString("idTypeProduct");
        String name =packageFromCaller.getString("typeProduct");


        etIdType.setText(IdStu);
        etTypeProduct.setText(name);



        btnEdit = (Button) findViewById(R.id.btnEditTypeProduct);

        // button click event
        btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                new EditTypeProdutDetails().execute();
            }
        });

        btnBack = (Button) findViewById(R.id.btnBack);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Edit_Type_Product.this , MainTypeProductActivity.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Execute", Toast.LENGTH_SHORT).show();
            }
        });


    }

    class EditTypeProdutDetails extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Edit_Type_Product.this);
            pDialog.setMessage("Edit Provider ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Saving product
         * */
        protected String doInBackground(String... args) {

            // getting updated data from EditTexts
            String idType = etIdType.getText().toString();
            String TypeProduct = etTypeProduct.getText().toString();


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("idProvider", idType));
            params.add(new BasicNameValuePair("nameOfCountry", TypeProduct));



            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONArray json = jsonParser.makeHttpRequestArray(url_update_type_product + "/" + idType + "/" + TypeProduct ,
                    "GET", params);

            Intent i = new Intent(getApplicationContext(), MainTypeProductActivity.class);
            startActivity(i);

            // closing this screen
            finish();
            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            pDialog.dismiss();
        }
    }

}
