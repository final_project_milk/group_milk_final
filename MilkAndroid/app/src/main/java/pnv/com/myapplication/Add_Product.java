package pnv.com.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/25/2015.
 */
public class Add_Product extends Activity {
    Button btnAdd, btnBack;
    TextView tvIdProduct;
    ImageView image;
    EditText etName, etPrice, etQuantity, etProductionDate, etExpireDate;

    JSONParser jsonParser = new JSONParser();
    private ProgressDialog pDialog;

    private static String url_create_product="http://192.168.56.1:8084/MilkGroupAnd/api/typeproduct/add";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product);

        // Edit Text
        image = (ImageView) findViewById(R.id.thumbnail);
        etName=(EditText) findViewById(R.id.etName);
        etPrice=(EditText) findViewById(R.id.etPrice);
        etQuantity=(EditText) findViewById(R.id.etQuantity);
        etProductionDate = (EditText) findViewById(R.id.etProductionDate);
        etExpireDate = (EditText) findViewById(R.id.etExpireDate);

        Button btnCreateProduct = (Button) findViewById(R.id.btAdd);

        // button click event
        btnCreateProduct.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // creating new product in background thread
                new CreateNewProduct().execute();
            }
        });

        btnBack = (Button) findViewById(R.id.btBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Add_Product.this, MainProductActivity.class);
                startActivity(intent);
            }
        });
    }

    class CreateNewProduct extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
         //   pDialog = new ProgressDialog(MainActivity.this);
//            pDialog.setMessage("Creating Product..");
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(true);
//            pDialog.show();

        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {

            String name = etName.getText().toString();
            float price = Float.parseFloat(etPrice.getText().toString());
            int quantity = Integer.parseInt(etQuantity.getText().toString());
            String ProductionDate = etProductionDate.getText().toString();
            String expireDate = etExpireDate.getText().toString();
         //   String image =




            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("nameOfCountry", name));


            JSONArray json = null;

            json = jsonParser.makeHttpRequestArray(url_create_product + "/images/"+image +"/" + name + "/" + price  + "/" + quantity
                    +"/" + ProductionDate + "/" + expireDate,
                    "GET", params);

            Log.i("Data add after: ", params.toString());
            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag


            Intent i = new Intent(getApplicationContext(), MainProductActivity.class);
            startActivity(i);

            // closing this screen
            finish();


            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * *
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
        }

    }
}
