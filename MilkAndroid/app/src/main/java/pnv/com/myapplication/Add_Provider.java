package pnv.com.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/23/2015.
 */
public class Add_Provider extends Activity {

    JSONParser jsonParser = new JSONParser();
    EditText inputName;

    // EditText inputDesc;

    private ProgressDialog pDialog;

    private static String url_create_student="http://192.168.56.1:8084/MilkGroupAnd/api/provider/add";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_provider);

        // Edit Text
        inputName = (EditText) findViewById(R.id.etName);

        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Add_Provider.this, MainProviderActivity.class);
                startActivity(intent);
            }
        });

        Button btnCreateStudent = (Button) findViewById(R.id.btnSubmit);

        // button click event
        btnCreateStudent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // creating new product in background thread
                new CreateNewStudent().execute();
            }
        });
    }

    class CreateNewStudent extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Add_Provider.this);
            pDialog.setMessage("Creating Product..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {

            String providerName = inputName.getText().toString();



            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("nameOfCountry", providerName));


            JSONArray json = null;

                json = jsonParser.makeHttpRequestArray(url_create_student + "/" + providerName ,
                        "GET", params);

            Log.i("Data add after: ", params.toString());
            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag


            Intent i = new Intent(getApplicationContext(), MainProviderActivity.class);
            startActivity(i);

            // closing this screen
            finish();


            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * *
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
        }

    }
}
