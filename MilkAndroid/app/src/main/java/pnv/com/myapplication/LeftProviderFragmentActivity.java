package pnv.com.myapplication;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/20/2015.
 */
public class LeftProviderFragmentActivity extends Fragment {



    private static String url_all_providers = "http://192.168.56.1:8084/MilkGroupAnd/api/provider";
    private static final String TAG_ID = "idProvider";
    private static final String TAG_NAMEPROVIDER = "nameOfCountry";


    private ProgressDialog pDialog;
    ArrayList<Provider> rowProvider =  new ArrayList<Provider>();

    JSONParser jParser = new JSONParser();

    ListView lv;

    JSONArray provider = null;
    MyArrayAdapterProvider adapter;

    private int posSt = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_left_provider, container, false);

        lv =(ListView) rootView.findViewById(R.id.list_left_provider);

        new LoadAllProducts().execute();


        return rootView;

    }





     class LoadAllProducts extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading providers. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
         protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
           // Log.i("All Products: ", "before");
            JSONArray json = jParser.makeHttpRequestArray(url_all_providers, "GET", params);
           // Log.i("All Products: ", "after");

            JSONArray students = json;

            for (int i = 0; i < students.length(); i++) {
                try {
                    JSONObject student = students.getJSONObject(i);

                    Provider studentobj =  new Provider();

                    studentobj.setIdProvider(student.getString(TAG_ID).toString());
                    studentobj.setNameOfCountry(student.getString(TAG_NAMEPROVIDER).toString());

                    rowProvider.add(studentobj);



                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
            if(json != null){
                Log.d("All Students: ", json.toString());
            }


            return null;
        }

@Override
        protected void onPostExecute(String result) {
            // dismiss the dialog after getting all products

    MyArrayAdapterProvider adapter = new MyArrayAdapterProvider(getActivity() ,R.layout.row_provider_detail, rowProvider);


    lv.setAdapter(adapter);
    registerForContextMenu(lv);
    lv.setOnItemClickListener (new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> arg0,
                                View arg1,
                                int arg2,
                                long arg3) {
           // Toast.makeText(getActivity(), "hello", Toast.LENGTH_SHORT).show();

//            FragmentManager fm = getFragmentManager();
//            FragmentTransaction ft = fm.beginTransaction();
//            RightStudentFragmentActivity llf = new RightStudentFragmentActivity();
//            llf.getStudent(rowStudent.get(arg2).getId(),rowStudent.get(arg2).getNameStudent(), rowStudent.get(arg2).getClassStudent());
//
//            ft.replace(R.id.fragment2, llf);
//
//            ft.commit();

////                Intent intent = new Intent(getActivity(), DisplayMessageActivity.class);
////                startActivity(intent);
//


            //menu use
//            Toast.makeText(getActivity(), "hello", Toast.LENGTH_SHORT).show();
//            Intent myIntent=new Intent(getActivity(), Detail_Right_Student.class);
//
//            Bundle bundle=new Bundle();
//            Student items = rowStudent.get(arg2);
//            bundle.putString("IdStudent", items.getId());
//            bundle.putString("Name", items.getNameStudent());
//
//            // bundle.putInt("Class", items.getClassStudent());
//            bundle.putString("Class_", items.getClassStudent());
//
//
//            myIntent.putExtra("PackageGiveData", bundle);
//
//            startActivity(myIntent);
//


            //creat demo fragment to fragment
            Provider items = rowProvider.get(arg2);
            RightProviderFragmentActivity newFragment  = (RightProviderFragmentActivity) getFragmentManager().findFragmentById(R.id.fragmentRightProvider);
            newFragment.change(items.getIdProvider().toString(), items.getNameOfCountry().toString());
        }
    });

    lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            //Toast.makeText(getActivity(), " "+ position, Toast.LENGTH_SHORT).show();
            posSt = position;
           // Toast.makeText(getActivity(), " "+ posSt, Toast.LENGTH_SHORT).show();
            return false;
        }
    });

    pDialog.dismiss();
        }

    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub

        menu.setHeaderIcon(R.drawable.ic_lanuncher);
        menu.setHeaderTitle("Edit Item");

        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);

    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case R.id.view:
                Intent intent = new Intent(getActivity(),
                        MainProviderActivity.class);
                startActivity(intent);
                break;
            case R.id.edit:
                Intent myIntent=new Intent(getActivity(), Edit_Provider.class);

                Bundle bundle=new Bundle();
                Provider items = rowProvider.get(posSt);
                bundle.putString("idProvider", items.getIdProvider().toString());
                bundle.putString("nameOfCountry", items.getNameOfCountry().toString());


                myIntent.putExtra("PackageGiveData", bundle);

                startActivity(myIntent);
                break;
            case R.id.delete:
                idDelete = rowProvider.get(posSt).getIdProvider().toString();
                new DeleteStudent().execute();
                break;

            default:
                break;
        }
        return super.onContextItemSelected(item);
    }


    private String idDelete;
    JSONParser jsonParser = new JSONParser();
    private static final String url_delete_student = "http://192.168.56.1:8084/MilkGroupAnd/api/provider/delete";

    class DeleteStudent extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Deleting Provider...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Deleting product
         * */
        protected String doInBackground(String... args) {


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("pid", idDelete));

            // getting product details by making HTTP request
            JSONArray json = jsonParser.makeHttpRequestArray(
                    url_delete_student + "/" + idDelete, "GET", params);

            // check your log for json response
            Log.d("Delete Student", json.toString());

            Intent i = new Intent(getActivity(), MainProviderActivity.class);
            startActivity(i);

            // closing this screen
            // finish();
            return null;

        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product deleted
            pDialog.dismiss();

        }

    }
}
