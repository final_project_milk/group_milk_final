package pnv.com.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/24/2015.
 */
public class Add_Type_Product extends Activity {

    EditText inputTypeProduct;

    JSONParser jsonParser = new JSONParser();
    private ProgressDialog pDialog;

    private static String url_create_type_product="http://192.168.56.1:8084/MilkGroupAnd/api/typeproduct/add";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_type_product);

        // Edit Text
        inputTypeProduct = (EditText) findViewById(R.id.etTypeProduct);

        Button btnCreateStudent = (Button) findViewById(R.id.btnSubmit);

        // button click event
        btnCreateStudent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // creating new product in background thread
                new CreateNewTypeProduct().execute();
            }
        });

        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Add_Type_Product.this, MainTypeProductActivity.class);
                startActivity(intent);
            }
        });
    }

    class CreateNewTypeProduct extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Add_Type_Product.this);
            pDialog.setMessage("Creating Product..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {

            String TypeProduct = inputTypeProduct.getText().toString();



            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("nameOfCountry", TypeProduct));


            JSONArray json = null;

            json = jsonParser.makeHttpRequestArray(url_create_type_product + "/" + TypeProduct ,
                    "GET", params);

            Log.i("Data add after: ", params.toString());
            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag


            Intent i = new Intent(getApplicationContext(), MainTypeProductActivity.class);
            startActivity(i);

            // closing this screen
            finish();


            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * *
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
        }

    }
}
