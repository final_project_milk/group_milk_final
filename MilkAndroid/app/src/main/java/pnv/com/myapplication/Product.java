package pnv.com.myapplication;

import java.io.Serializable;

/**
 * Created by linhnn on 5/11/2015.
 */
public class Product implements Serializable {
    private String image;
    private String idProduct;
    private String name;
    private String price;
    private String quantity;
    private String productionDate;
    private String expireDate;
    private String idProvider;
    private String idTypeProduct;


    public Product(String image, String idProduct,
                   String name, String price,
                   String quantity, String productionDate,
                   String expireDate) { //, String idProvider, String idTypeProduct
        this.image = image;
        this.idProduct = idProduct;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.productionDate = productionDate;
        this.expireDate = expireDate;
        //this.idProvider = idProvider;
       // this.idTypeProduct = idTypeProduct;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(String idProvider) {
        this.idProvider = idProvider;
    }

    public String getIdTypeProduct() {
        return idTypeProduct;
    }

    public void setIdTypeProduct(String idTypeProduct) {
        this.idTypeProduct = idTypeProduct;
    }

    @Override
    public String toString() {
        return this.idProduct + " " + this.name+ "  "+this.price ;
    }
}
