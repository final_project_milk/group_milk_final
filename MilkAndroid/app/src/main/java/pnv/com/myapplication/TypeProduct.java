package pnv.com.myapplication;

/**
 * Created by linhnn on 5/24/2015.
 */
public class TypeProduct {
    private String idTypeProduct;
    private String typeProduct;

    public String getIdTypeProduct() {
        return idTypeProduct;
    }

    public void setIdTypeProduct(String idTypeProduct) {
        this.idTypeProduct = idTypeProduct;
    }

    public String getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(String typeProduct) {
        this.typeProduct = typeProduct;
    }
}
