package pnv.com.myapplication;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linhnn on 5/24/2015.
 */
public class LeftTypeProductFragmentActivity extends Fragment {

    ListView lv;

    private static String url_all_type_product = "http://192.168.56.1:8084/MilkGroupAnd/api/typeproduct";
    private static final String TAG_ID = "idTypeProduct";
    private static final String TAG_TYPE_PRODUCT = "typeProduct";

    private ProgressDialog pDialog;

    JSONParser jParser = new JSONParser();
    ArrayList<TypeProduct> rowTypeProduct =  new ArrayList<TypeProduct>();

    MyArrayAdapterTypeProduct adapterTypeProduct;

    private int posSt = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_left_type_product, container, false);

        lv =(ListView) rootView.findViewById(R.id.list_left_type_product);

        new LoadAllTypeProduct().execute();

        return rootView;

    }


    class LoadAllTypeProduct extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading providers. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            // Log.i("All Products: ", "before");
            JSONArray json = jParser.makeHttpRequestArray(url_all_type_product, "GET", params);
            // Log.i("All Products: ", "after");

            JSONArray typeProduct = json;

            for (int i = 0; i < typeProduct.length(); i++) {
                try {
                    JSONObject student = typeProduct.getJSONObject(i);

                    TypeProduct typeProductObj =  new TypeProduct();

                    typeProductObj.setIdTypeProduct(student.getString(TAG_ID).toString());
                    typeProductObj.setTypeProduct(student.getString(TAG_TYPE_PRODUCT).toString());

                    rowTypeProduct.add(typeProductObj);



                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
            if(json != null){
                Log.d("All type Product: ", json.toString());
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // dismiss the dialog after getting all products

            MyArrayAdapterTypeProduct adapter = new MyArrayAdapterTypeProduct(getActivity() ,R.layout.row_type_product_detail, rowTypeProduct);


            lv.setAdapter(adapter);
            registerForContextMenu(lv);
            lv.setOnItemClickListener (new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0,
                                        View arg1,
                                        int arg2,
                                        long arg3) {


                    TypeProduct items = rowTypeProduct.get(arg2);
                    RightTypeProductFragmentActivity newFragment  = (RightTypeProductFragmentActivity) getFragmentManager().findFragmentById(R.id.fragmentRightTypeProduct);
                    newFragment.change(items.getIdTypeProduct().toString(), items.getTypeProduct().toString());

                }
            });

            lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(getActivity(), " "+ position, Toast.LENGTH_SHORT).show();
                    posSt = position;
                    // Toast.makeText(getActivity(), " "+ posSt, Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            pDialog.dismiss();
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub

        menu.setHeaderIcon(R.drawable.ic_lanuncher);
        menu.setHeaderTitle("Edit Item");

        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);

    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case R.id.view:
                Intent intent = new Intent(getActivity(),
                        MainTypeProductActivity.class);
                startActivity(intent);
                break;
            case R.id.edit:
                Intent myIntent=new Intent(getActivity(), Edit_Type_Product.class);

                Bundle bundle=new Bundle();
                TypeProduct items = rowTypeProduct.get(posSt);

                bundle.putString("idTypeProduct", items.getIdTypeProduct().toString());
                bundle.putString("typeProduct", items.getTypeProduct().toString());


                myIntent.putExtra("PackageGiveData", bundle);

                startActivity(myIntent);
                break;
            case R.id.delete:
                idDelete = rowTypeProduct.get(posSt).getIdTypeProduct().toString();
                new DeleteTypeProduct().execute();
                break;

            default:
                break;
        }
        return super.onContextItemSelected(item);
    }


    private String idDelete;
    JSONParser jsonParser = new JSONParser();
    private static final String url_delete_type_product = "http://192.168.56.1:8084/MilkGroupAnd/api/typeproduct/delete";

    class DeleteTypeProduct extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Deleting Provider...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Deleting product
         * */
        protected String doInBackground(String... args) {


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("pid", idDelete));

            // getting product details by making HTTP request
            JSONArray json = jsonParser.makeHttpRequestArray(
                    url_delete_type_product + "/" + idDelete, "GET", params);

            // check your log for json response
            Log.d("Delete Student", json.toString());

            Intent i = new Intent(getActivity(), MainTypeProductActivity.class);
            startActivity(i);

            // closing this screen
            // finish();
            return null;

        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product deleted
            pDialog.dismiss();

        }

    }

}
