package pnv.com.myapplication;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.List;

/**
 * Created by linhnn on 5/20/2015.
 */
public class MyArrayAdapterProduct extends ArrayAdapter<Product> {

    Activity context;
    List<Product> myArray= null;
    int layoutId;
   // ArrayList<HashMap<String, String>> myArray;

    public MyArrayAdapterProduct(Activity context, int resource, List<Product> arr) {
        super(context, resource, arr);
        this.context = context;
        this.layoutId = resource;
        this.myArray = arr;
    }


    public View getView(int position, View convertView,
                        ViewGroup parent) {

        LayoutInflater inflater=
                context.getLayoutInflater();
        convertView=inflater.inflate(layoutId, null);

        if(myArray.size()>0 && position>=0) {
            Log.d("Postion :", String.valueOf(position));
            Product product = myArray.get(position);

            TextView tvId = (TextView) convertView.findViewById(R.id.tvIdProduct);
            TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            TextView tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
            TextView tvQuantity = (TextView) convertView.findViewById(R.id.tvQuantity);
            TextView tvProductiondate = (TextView) convertView.findViewById(R.id.tvProductionDate);
            TextView tvExpireDate = (TextView) convertView.findViewById(R.id.tvExpireDate);


            ImageView img = (ImageView) convertView.findViewById(R.id.thumbnail);


            tvId.setText(product.getIdProduct());
            tvName.setText(product.getName());
            tvPrice.setText(product.getPrice());
            tvQuantity.setText(product.getQuantity());
            tvProductiondate.setText(product.getProductionDate());
            tvExpireDate.setText(product.getExpireDate());
            new DownloadImageTask(img).execute(product.getImage());

        }

        return convertView;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}
