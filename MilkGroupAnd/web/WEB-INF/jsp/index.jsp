
<%@page import="com.pnv.dao.ProductDaoIml"%>
<%@page import="com.pnv.models.Product"%>
<%@page import="java.util.List"%>
<%@ include file="./header.jsp" %>


<div class="body">

<%--<img src="<c:url value="/resources/image/hinhnen.jpg" />" height="500" width="1000"/>--%>

<%--Phantrang--%>
    <%
        ProductDaoIml productDao = new ProductDaoIml();
        List<Product> listProduct = productDao.findAll();
        String page1 = "";
        String page2 = "";
        int start = 0;
        int end;
        if (listProduct.size() < 16) {
            end = listProduct.size();
        } else {
            end = 4;
        }
        if (request.getParameter("start") != null) {
            page1 = request.getParameter("start");
            start = Integer.parseInt(page1);
        }
        if (request.getParameter("end") != null) {
            page2 = request.getParameter("end");
            end = Integer.parseInt(page2);
        }
        List<Product> list = productDao.getSub(listProduct, start, end);
    %>

    

    
    <div id="site-wrapper">
        <ul class="products homepage">
            <%for (Product p : list) {%>
           <li class="preorder"><span class="tagimg "></span> <a
				href="detail.jsp?ma_san_pham=<%=p.getIdProduct()%>"> <img
					src="resources/<%=p.getImage()%>" width=" 250px" height="250px" />
					<h3><%=p.getName()%></h3>
					<h4>
						Price:
						<%=p.getPrice()%>
						VN?
					</h4> <span class="textkm">Promotion: <strong>10 %</strong>
				</span>
					<p class="info">
						<span>Quantity: <%=p.getQuantity()%></span> <span>P Date: <%=p.getProductionDate()%></span>
						<span>E Date: <%=p.getExpireDate()%></span>
					</p>
			</a></li>
                <%}
                %>
        </ul>
    </div>
        
        <div style="clear: both;" </div>
        
    <%
        int limit = listProduct.size() / 4;
        if (limit * 4 < listProduct.size()) {
            limit += 1;
        }
        for (int i = 1; i <= limit; i++) {
            int a = (i - 1) * 4;
            int b = i * 4;
            if (b > listProduct.size()) {
                b = listProduct.size();
            }
    %>
    <a href="?start=<%=a%>&end=<%=b%>"><%=i%></a>
    <%
        }
    %>
</div>



<%@ include file="./footer.jsp" %>