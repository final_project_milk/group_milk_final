<%-- 
    Document   : typeproduct-add
    Created on : May 13, 2015, 4:54:10 PM
    Author     : thangnd
--%>

<%@ include file="./header.jsp" %>

<div class="body">
<h1>Add New Typeproduct</h1>

<div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="typeproductForm" method="POST" >
            <form:hidden path="idTypeProduct" />
            <tr>
                <td align="left" width="20%">Type Product: </td>
                <td align="left" width="40%"><form:input path="typeProduct" size="30"/></td>
                <td align="left"><form:errors path="typeProduct" cssClass="error"/></td>
            </tr>
            

                    <tr>
                        <td><a href="<%=request.getContextPath()%>/typeproduct" class="button">Back</a></td>
                        <td align="center"><input type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
            </div>
</div>
<%@ include file="./footer.jsp" %>