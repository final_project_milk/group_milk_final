<%-- 
    Document   : resultEdit
    Created on : May 18, 2015, 4:46:11 PM
    Author     : thangnd
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>
            Id : ${productEdit.idProduct} </br>
            Image : ${productEdit.image} </br>
            Name : ${productEdit.name} <br/>
            Price : ${productEdit.price} </br>
            Quantity : ${productEdit.quantity} </br>
            P Date : ${productEdit.productionDate} <br/>
            E Date: ${productEdit.expireDate}
        </p>
    </body>
</html>
