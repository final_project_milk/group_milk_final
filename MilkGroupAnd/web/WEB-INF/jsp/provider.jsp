<%-- 
    Document   : provider
    Created on : May 13, 2015, 4:19:13 PM
    Author     : thangnd
--%>

<%@ include file="./header.jsp" %>

<div class="body">
<h1>Provider Management</h1>
<h3>${add_success}</h3>
<a href="<%=request.getContextPath()%>/provider/add" class="button">Add New</a>

<table border="1">
    <tr>
        <th>Id</th>
        <th>Provider Name of Country</th>
        <th>Action</th>
        <c:forEach items="${providers_list}" var="provider">  
        <tr>  
            <td><c:out value="${provider.idProvider}"/></td>  
            <td><c:out value="${provider.nameOfCountry}"/></td>  
            
           
            <td align="center"><a href="<%=request.getContextPath()%>/provider/edit?idProvider=${provider.idProvider}">Edit</a> | <a href="<%=request.getContextPath()%>/provider/delete?idProvider=${provider.idProvider}">Delete</a></td>  
        </tr>  
    </c:forEach> 
</tr>

</table>
</div>
<%@ include file="./footer.jsp" %>
