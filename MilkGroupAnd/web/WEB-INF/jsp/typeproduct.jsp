<%-- 
    Document   : typeproduct
    Created on : May 13, 2015, 4:59:03 PM
    Author     : thangnd
--%>

<%@ include file="./header.jsp" %>

<div class="body">
<h1>Type Product Management</h1>
<h3>${add_success}</h3>
<a href="<%=request.getContextPath()%>/typeproduct/add" class="button">Add New</a>

<table border="1">
    <tr>
        <th>Id</th>
        <th>Type Product</th>
        <th>Action</th>
        <c:forEach items="${typeproduct_list}" var="typeproduct">  
        <tr>  
            <td><c:out value="${typeproduct.idTypeProduct}"/></td>
            <td><c:out value="${typeproduct.typeProduct}"/></td>  
           
           
            <td align="center"><a href="<%=request.getContextPath()%>/typeproduct/edit?idTypeProduct=${typeproduct.idTypeProduct}">Edit</a> | <a href="<%=request.getContextPath()%>/typeproduct/delete?idTypeProduct=${typeproduct.idTypeProduct}">Delete</a></td>  
        </tr>  
    </c:forEach> 
</tr>

</table>
</div>
<%@ include file="./footer.jsp" %>
