<%-- 
    Document   : product-add
    Created on : May 18, 2015, 2:42:00 PM
    Author     : thangnd
--%>





<%@ include file="./header.jsp" %>
<div class="body">

<h1>Add New Product</h1>

<div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="productForm" method="POST" >
            <form:hidden path="idProduct" />
            <tr>
                <td align="left" width="20%">Image: </td>
                <td align="left" width="40%"><form:input path="image" size="30"/></td>
                <td align="left"><form:errors path="image" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Name: </td>
                <td><form:input path="name" size="30"/></td>
                <td><form:errors path="name" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Price: </td>
                <td><form:input path="price" size="30"/></td>
                <td><form:errors path="price" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Quantity: </td>
                <td><form:input path="quantity" size="30"/></td>
                <td><form:errors path="quantity" cssClass="error"/></td>
            </tr>
            <tr>
                <td>P Date: </td>
                <td><form:input path="productionDate" size="30"/></td>
                <td><form:errors path="productionDate" cssClass="error"/></td>
            </tr>
            <tr>
                <td>E Date: </td>
                <td><form:input path="expireDate" size="30"/></td>
                <td><form:errors path="expireDate" cssClass="error"/></td>
            </tr>
           <tr>
                <td>Provider :  </td>
                <td><form:select path="provider.idProvider">
            		<form:option value="" label="--Please Select"/>
            		<form:options items="${provider_lst}" itemValue="idProvider" itemLabel="nameOfCountry"/>
        		</form:select></td>
                <td><form:errors path="provider.idProvider" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Type Product :  </td>
                <td><form:select path="typeproduct.idTypeProduct">
            		<form:option value="" label="--Please Select"/>
            		<form:options items="${typeproduct_lst}" itemValue="idTypeProduct" itemLabel="typeProduct"/>
        		</form:select></td>
                <td><form:errors path="typeproduct.idTypeProduct" cssClass="error"/></td>
            </tr>

                    <tr>
                        <td><a href="<%=request.getContextPath()%>/product" class="button">Back</a></td>
                        <td align="center"><input type="submit" value="Submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
            </div>
</div>
<%@ include file="./footer.jsp" %>