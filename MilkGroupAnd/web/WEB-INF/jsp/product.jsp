<%-- 
    Document   : product
    Created on : May 13, 2015, 4:23:31 PM
    Author     : thangnd
--%>

<%@page import="com.pnv.dao.ProductDaoIml"%>
<%@page import="com.pnv.models.Product"%>
<%@page import="java.util.List"%>
<%@ include file="./header.jsp" %>

<div class="body">

    
    <h3>${add_success}</h3>
    <a href="<%=request.getContextPath()%>/product/add" class="button">Add New</a>
    <%--Phantrang--%>
    <%
        ProductDaoIml productDao = new ProductDaoIml();
        List<Product> listProduct = productDao.findAll();
        String page1 = "";
        String page2 = "";
        int start = 0;
        int end;
        if (listProduct.size() < 16) {
            end = listProduct.size();
        } else {
            end = 8;
        }
        if (request.getParameter("start") != null) {
            page1 = request.getParameter("start");
            start = Integer.parseInt(page1);
        }
        if (request.getParameter("end") != null) {
            page2 = request.getParameter("end");
            end = Integer.parseInt(page2);
        }
        List<Product> list = productDao.getSub(listProduct, start, end);
    %>


    <%
        int limit = listProduct.size() / 8;
        if (limit * 8 < listProduct.size()) {
            limit += 1;
        }
        for (int i = 1; i <= limit; i++) {
            int a = (i - 1) * 8;
            int b = i * 8;
            if (b > listProduct.size()) {
                b = listProduct.size();
            }
    %>
    <a href="?start=<%=a%>&end=<%=b%>"><%=i%></a>
    <%
        }
    %>


  <%-- <div id="site-wrapper">
        <ul class="products homepage">
            <%for (Product product : list) {%>
            
            <li class="preorder"><span class="tagimg"></span><a
                     <img
                        src="sanpham/<%=product.getImage()%>" width=" 250px" height="250px" />
                    <h3><%=product.getName()%></h3>
                    <h4>
                        Gi�:
                        <%=product.getPrice()%>
                        VN?
                    </h4> <span class="textkm">Khuyen mai: <strong><%=product.getIdProduct()%></strong>
                        
                    </span>
                    <p class="info">
                        <span>QTY: <%=product.getQuantity()%></span> 
                        <span>CPU: <%=product.getProductionDate()%></span>
                        <span>Camera: <%=product.getExpireDate()%>
                    </p>
                </a></li>
                <%}
                %>
        </ul>
    </div>--%>
     <table border="1">
        <tr>

            <th>ID</th>
            <th>Image</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Production Date</th>
            <th>Expire Date</th>
            <th>Action</th>
                <c:forEach items="${product_list}" var="product">  
            <tr>  

                <td><c:out value="${product.idProduct}"/></td>  
                <td><c:out value="${product.image}"/></td>  
                <td><c:out value="${product.name}"/></td>
                <td><c:out value="${product.price}"/></td>  
                <td><c:out value="${product.quantity}"/></td>  
                <td><c:out value="${product.productionDate}"/></td>  
                <td><c:out value="${product.expireDate}"/></td> 
                <td align="center"><a href="<%=request.getContextPath()%>/product/edit?idProduct=${product.idProduct}">Edit</a> | <a href="<%=request.getContextPath()%>/product/delete?idProduct=${product.idProduct}">Delete</a></td>  
            </tr>  
        </c:forEach> 

        </tr>

    </table>
   
</div>
<%@ include file="./footer.jsp" %>
