<%-- 
    Document   : provider-add
    Created on : May 13, 2015, 4:14:55 PM
    Author     : thangnd
--%>

<%@ include file="./header.jsp" %>

<div class="body">
<h1>Add New Provider</h1>

<div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="providerForm" method="POST" >
            <form:hidden path="idProvider" />
            <tr>
                <td align="left" width="20%">Provider Name of Country: </td>
                <td align="left" width="40%"><form:input path="nameOfCountry" size="30"/></td>
                <td align="left"><form:errors path="nameOfCountry" cssClass="error"/></td>
            </tr>
            

                    <tr>
                        <td><a href="<%=request.getContextPath()%>/provider" class="button">Back</a></td>
                        <td align="center"><input type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
            </div>
</div>
<%@ include file="./footer.jsp" %>
