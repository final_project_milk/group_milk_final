/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.ProductDao;
import com.pnv.dao.ProviderDao;
import com.pnv.dao.TypeproductDao;
import com.pnv.models.Product;
import com.pnv.models.Provider;
import com.pnv.models.Typeproduct;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author thangnd
 */

//@Controller
//@RequestMapping(value = "/product")
//public class ProductControllers {
//
//    @Autowired
//    private ProductDao productDao;
//    @Autowired
//    private ProviderDao providerDao;
//    @Autowired
//    private TypeproductDao typeproductDao;
//
//    @RequestMapping(method = RequestMethod.GET)
//    public String viewEmployeePage(@RequestParam(value = "typeproductId", required = false) Integer typeproductId, @RequestParam(value = "providerId", required = false) Integer providerId, ModelMap map) {
//
//        List<Product> product_list;
//
//        if (typeproductId != null) {
//            product_list = typeproductDao.findByTypeproductId(typeproductId).getProducts();
//        } else if (providerId != null) {
//            product_list = providerDao.findbyProviderId(providerId).getProducts();
//        } else {
//            product_list = productDao.findAll();
//        }
//
//        map.put("product_list", product_list);
//        return "product";
//    }
//
//    @RequestMapping(value = "/add", method = RequestMethod.GET)
//    public String viewAddNewPage(ModelMap map) {
//
//        List<Typeproduct> typeproduct_lst = typeproductDao.findAll();
//        map.addAttribute("typeproduct_lst", typeproduct_lst);
//        List<Provider> providerForm = providerDao.findAll();
//        map.addAttribute("product_lst", providerForm);
//
//        map.addAttribute("productForm", new Product());
//
//        return "product-add";
//
//    }
//
//    @RequestMapping(value = "/add", method = RequestMethod.POST)
//    public String doAddNew(@Valid @ModelAttribute("productForm") Product productForm,
//            BindingResult result, ModelMap map) {
//
//        if (result.hasErrors()) {
//            List<Typeproduct> typeproduct_lst = typeproductDao.findAll();
//            map.addAttribute("typeproduct_lst", typeproduct_lst);
//            List<Provider> providerForm = providerDao.findAll();
//            map.addAttribute("providers_lst", providerForm);
//            map.addAttribute("productForm", productForm);
//
//            return "product-add";
//        }
//
//        productDao.saveOrUpdate(productForm);
//
//        /**
//         * Get all titles
//         */
//        List<Product> product_list = productDao.findAll();
//        map.put("product_list", product_list);
//        map.addAttribute("add_success", "ok");
//
//        return "product";
//    }
//
//    @RequestMapping(value = "/edit", method = RequestMethod.GET)
//    public String viewEditTitlePage(@RequestParam(value = "idProduct", required = true) int idProduct, ModelMap map) {
//        List<Typeproduct> typeproduct_lst = typeproductDao.findAll();
//        map.addAttribute("typeproduct_lst", typeproduct_lst);
//        List<Provider> providerForm = providerDao.findAll();
//        map.addAttribute("providers_lst", providerForm);
//        Product productForm = productDao.findByProductId(idProduct);
//        map.addAttribute("productForm", productForm);
//        return "product-add";
//    }
//
//    @RequestMapping(value = "/delete", method = RequestMethod.GET)
//    public String doDeleteTitle(@RequestParam(value = "idProduct", required = true) int idProduct, ModelMap map) {
//
//        productDao.delete(productDao.findByProductId(idProduct));
//
//        return Constant.REDIRECT + "/product";
//
//    }




@Controller
@RequestMapping(value = "/product")
public class ProductControllers {

    @Autowired
    private ProductDao productDao;
    @Autowired
    private ProviderDao providerDao;
    @Autowired
    private TypeproductDao typeproductDao;

    @RequestMapping(method = RequestMethod.GET)
    public String viewEmployeePage(@RequestParam(value = "titleId", required = false) Integer titleId, @RequestParam(value = "providerId", required = false) Integer providerId, ModelMap map) {

        List<Product> product_list;

        if (titleId != null) {
            product_list = typeproductDao.findByTypeproductId(titleId).getProducts();
        } else if (providerId != null) {
            product_list = providerDao.findbyProviderId(providerId).getProducts();
        } else {
            product_list = productDao.findAll();
        }

        map.put("product_list", product_list);
        return "product";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        List<Typeproduct> typeproduct_lst = typeproductDao.findAll();
        map.addAttribute("typeproduct_lst", typeproduct_lst);
        List<Provider> providerForm = providerDao.findAll();
        map.addAttribute("provider_lst", providerForm);

        map.addAttribute("productForm", new Product());

        return "product-add";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("providerForm") Product productForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            List<Typeproduct> typeproduct_lst = typeproductDao.findAll();
            map.addAttribute("typeproduct_lst", typeproduct_lst);
            List<Provider> providerForm = providerDao.findAll();
            map.addAttribute("provider_lst", providerForm);
            map.addAttribute("productForm", productForm);

            return "product-add";
        }

        productDao.saveOrUpdate(productForm);

        /**
         * Get all titles
         */
        List<Product> product_list = productDao.findAll();
        map.put("product_list", product_list);
        map.addAttribute("add_success", "ok");

        return "product";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "idProduct", required = true) int idProduct, ModelMap map) {
        List<Typeproduct> typeproduct_lst = typeproductDao.findAll();
        map.addAttribute("typeproduct_lst", typeproduct_lst);
        List<Provider> providerForm = providerDao.findAll();
        map.addAttribute("provider_lst", providerForm);
        Product productForm = productDao.findByProductId(idProduct);
        map.addAttribute("productForm", productForm);
        return "product-add";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "idProduct", required = true) int idProduct, ModelMap map) {

        productDao.delete(productDao.findByProductId(idProduct));

        return Constant.REDIRECT + "/product";

    }
}
