/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.ProviderDao;
import com.pnv.dao.TypeproductDao;
import com.pnv.models.Provider;
import com.pnv.models.Typeproduct;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author thangnd
 */
//@Controller
//@RequestMapping(value = "/typeproduct")
//public class TypeproductController {
//     @Autowired
//    private TypeproductDao typeproductDao;
//     public TypeproductController() {}
//     @RequestMapping(method = RequestMethod.GET)
//    public String viewTypeproductPage(ModelMap map) {
//
//        /**
//         * Get all titles
//         */
//        List<Typeproduct> typeproduct_list = typeproductDao.findAll();
//        map.put("typeproduct_list", typeproduct_list);
//
//        return "typeproduct";
//    }
//      @RequestMapping(value = "/edit", method = RequestMethod.GET)
//    public String viewEditTypeproductPage(@RequestParam(value = "idTypeProduct", required = true) int idTypeProduct, ModelMap map) {
//
//        Typeproduct typeproductForm = typeproductDao.findByTypeproductId(idTypeProduct);
//        map.addAttribute("typeproductForm", typeproductForm);
//        return "typeproduct-add";
//    }
//      @RequestMapping(value = "/add", method = RequestMethod.GET)
//    public String viewAddNewPage(ModelMap map) {
//
//        Typeproduct typeproductForm = new Typeproduct();
//        map.addAttribute("typeproductForm", typeproductForm);
//        return "typeproduct-add";
//
//    }
//      @RequestMapping(value = "/add", method = RequestMethod.POST)
//    public String doAddNew(@Valid @ModelAttribute("typeproductForm") Typeproduct typeproductForm,
//            BindingResult result, ModelMap map) {
//
//        if (result.hasErrors()) {
//            map.addAttribute("typeproductForm", typeproductForm);
//            return "typeproduct-add";
//        }
//        typeproductDao.saveOrUpdate(typeproductForm);
//
//        /**
//         * Get all titles
//         */
//        List<Typeproduct> typeproduct_list = typeproductDao.findAll();
//        map.put("typeproduct_list", typeproduct_list);
//        map.addAttribute("add_success", "ok");
//
//        return "typeproduct";
//    }
//      @RequestMapping(value = "/delete", method = RequestMethod.GET)
//    public String doDeleteTypeproduct(@RequestParam(value = "typeproductForm", required = true) int typeproductForm, ModelMap map) {
//
//        typeproductDao.delete(typeproductDao.findByTypeproductId(typeproductForm));
//
//        return Constant.REDIRECT + "/typeproduct";
//
//    }



@Controller
@RequestMapping(value = "/typeproduct")
public class TypeproductController {
    @Autowired
    private TypeproductDao typeproductDao;
    public TypeproductController(){}
    @RequestMapping(method = RequestMethod.GET)
    public String viewDepartmentPage(ModelMap map) {
        /**
         * Get all titles
         */
        List<Typeproduct> typeproduct_list = typeproductDao.findAll();
        map.put("typeproduct_list", typeproduct_list);
        return "typeproduct";
    }
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTypeproductPage(@RequestParam(value = "idTypeProduct", required = true) int idTypeProduct, ModelMap map) {

        Typeproduct typeproductForm = typeproductDao.findByTypeproductId(idTypeProduct);
        map.addAttribute("typeproductForm", typeproductForm);
        return "typeproduct-add";
    }
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Typeproduct typeproductForm = new Typeproduct();
        map.addAttribute("typeproductForm", typeproductForm);
        return "typeproduct-add";

    }
   
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("typeproductForm") Typeproduct typeproductForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("typeproductForm", typeproductForm);
            return "typeproduct-add";
        }
        typeproductDao.saveOrUpdate(typeproductForm);

        /**
         * Get all titles
         */
        List<Typeproduct> typeproduct_list = typeproductDao.findAll();
        map.put("typeproduct_list", typeproduct_list);
        map.addAttribute("add_success", "ok");

        return "typeproduct";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteProvider(@RequestParam(value = "idTypeProduct", required = true) int idTypeProduct, ModelMap map) {

        typeproductDao.delete(typeproductDao.findByTypeproductId(idTypeProduct));

        return Constant.REDIRECT + "/typeproduct";

    }
}
