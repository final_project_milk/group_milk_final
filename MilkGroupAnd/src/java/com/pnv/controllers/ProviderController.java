/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.ProviderDao;
import com.pnv.models.Provider;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author thangnd
 */
//@Controller
//@RequestMapping(value = "/provider")
//public class ProviderController {
//    @Autowired
//    private ProviderDao providerDao;
//    public ProviderController(){}
//    @RequestMapping(method = RequestMethod.GET)
//    public String viewDepartmentPage(ModelMap map) {
//        /**
//         * Get all titles
//         */
//        List<Provider> provider_list = providerDao.findAll();
//        map.put("provider_list", provider_list);
//        return "provider";
//    }
//    @RequestMapping(value = "/edit", method = RequestMethod.GET)
//    public String viewEditProviderPage(@RequestParam(value = "idProvider", required = true) int idProvider, ModelMap map) {
//
//        Provider providerForm = providerDao.findbyProviderId(idProvider);
//        map.addAttribute("providerForm", providerForm);
//        return "provider-add";
//    }
//    @RequestMapping(value = "/add", method = RequestMethod.GET)
//    public String viewAddNewPage(ModelMap map) {
//
//        Provider providerForm = new Provider();
//        map.addAttribute("providerForm", providerForm);
//        return "provider-add";
//
//    }
//   
//    @RequestMapping(value = "/add", method = RequestMethod.POST)
//    public String doAddNew(@Valid @ModelAttribute("providerForm") Provider providerForm,
//            BindingResult result, ModelMap map) {
//
//        if (result.hasErrors()) {
//            map.addAttribute("providerForm", providerForm);
//            return "provider-add";
//        }
//        providerDao.saveOrUpdate(providerForm);
//
//        /**
//         * Get all titles
//         */
//        List<Provider> provider_list = providerDao.findAll();
//        map.put("provider_list", provider_list);
//        map.addAttribute("add_success", "ok");
//
//        return "provider";
//    }
//    @RequestMapping(value = "/delete", method = RequestMethod.GET)
//    public String doDeleteProvider(@RequestParam(value = "idProvider", required = true) int idProvider, ModelMap map) {
//
//        providerDao.delete(providerDao.findbyProviderId(idProvider));
//
//        return Constant.REDIRECT + "/provider";
//
//    }



@Controller
@RequestMapping(value = "/provider")
public class ProviderController {

    @Autowired
    private ProviderDao providerDao;

    public ProviderController() {
    }

    @RequestMapping(method = RequestMethod.GET)
    public String viewProviderPage(ModelMap map) {
        /**
         * Get all titles
         */
        List<Provider> providers_list = providerDao.findAll();
        map.put("providers_list", providers_list);
        return "provider";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "idProvider", required = true) int idProvider, ModelMap map) {

        Provider providerForm = providerDao.findbyProviderId(idProvider);
        map.addAttribute("providerForm", providerForm);
        return "provider-add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Provider providerForm = new Provider();
        map.addAttribute("providerForm", providerForm);
        return "provider-add";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("providerForm") Provider providerForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("providerForm", providerForm);
            return "provider-add";
        }
        providerDao.saveOrUpdate(providerForm);

        /**
         * Get all titles
         */
        List<Provider> providers_list = providerDao.findAll();
        map.put("providers_list", providers_list);
        map.addAttribute("add_success", "ok");

        return "provider";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "idProvider", required = true) int idProvider, ModelMap map) {

        providerDao.delete(providerDao.findbyProviderId(idProvider));

        return Constant.REDIRECT + "/provider";

    }
}
