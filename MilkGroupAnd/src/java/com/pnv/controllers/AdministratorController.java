/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author thangnd
 */
@Controller
public class AdministratorController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewWelcomePage(ModelMap map) {
        return "index";
    }
}
