/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Product;
import java.util.List;

/**
 *
 * @author thangnd
 */
public interface ProductDao {
    public List<Product> getSub(List<Product> list, int start, int end);
     public void saveOrUpdate(Product product);
     public void delete(Product product);
     public List<Product> findAll();
     public Product findByProductId(int idProduct);
     public List<Product> findByProductName(String productName);
     
}
