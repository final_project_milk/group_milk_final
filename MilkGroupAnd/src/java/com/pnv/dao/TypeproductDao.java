/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Typeproduct;
import java.util.List;

/**
 *
 * @author thangnd
 */
public interface TypeproductDao {
    public void saveOrUpdate(Typeproduct typeproduct);
     public void delete(Typeproduct typeproduct);
     public List<Typeproduct> findAll();
     public Typeproduct findByTypeproductId(int idTypeProduct);
     public Typeproduct findByTypeproductCode(String typeProduct);
}
