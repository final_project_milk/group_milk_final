/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Product;
import com.pnv.models.Provider;
import com.pnv.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author thangnd
 */
@Service
public class ProviderDaoImpl implements ProviderDao{
    //private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
//        public void saveOrUpdate(Product product) {
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
//        try {
//            session.saveOrUpdate(product);
//            transaction.commit();
//        } catch (HibernateException hb) {
//            transaction.rollback();
//            System.err.println("error" + hb);
//        } finally {
//            session.close();
//        }
//    }
//    @Override
    public void delete(Provider provider) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(provider);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }
    @Override
    public List<Provider> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Provider> productList = session.createQuery("from Provider").list();
        session.close();
        return productList;
    }
    @Override
    public Provider findbyProviderId(int idProvider) {
        Provider provider = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            provider = (Provider) session.get(Provider.class, idProvider);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return provider;
    }
    @Override
    public Provider findByProviderCountry(String ProviderCountry) {
        Provider provider = null;
        String strQuery = "from Provider WHERE nameOfCountry = :nameOfCountry ";
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("nameOfCountry", ProviderCountry);
        provider = (Provider) query.uniqueResult();
        session.close();
        return provider;
    }

    @Override
    public void saveOrUpdate(Provider provider) {
       Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(provider);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }
}
