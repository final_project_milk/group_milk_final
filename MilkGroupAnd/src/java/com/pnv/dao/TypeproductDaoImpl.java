/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Typeproduct;
import com.pnv.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thangnd
 */
@Repository("TypeproductDao")
public class TypeproductDaoImpl implements TypeproductDao{
    //private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    public TypeproductDaoImpl(){}
    @Override
    public void saveOrUpdate(Typeproduct typeproduct) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(typeproduct);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }
    @Override
    public void delete(Typeproduct typeproduct) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(typeproduct);
//            session.flush();
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }
    @Override
    public Typeproduct findByTypeproductId(int idTypeProduct) {
        Typeproduct title = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
         Transaction transaction = session.beginTransaction();
        try {
          title =  (Typeproduct) session.get(Typeproduct.class, idTypeProduct);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return title;
    }
    @Override
    public Typeproduct findByTypeproductCode(String typeProduct) {
        Typeproduct typeproduct = null;
        String strQuery = "from Typeproduct WHERE typeProduct =:typeProduct ";
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("typeProduct", typeProduct);
        typeproduct = (Typeproduct) query.uniqueResult();
        session.close();
        return typeproduct;
    }
    @Override
    public List<Typeproduct> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Typeproduct> typeproductList = session.createQuery("from Typeproduct").list();
        session.close();
        return typeproductList;

    }
}
