/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Provider;
import java.util.List;

/**
 *
 * @author thangnd
 */
public interface ProviderDao {
    public void saveOrUpdate(Provider provider);
    public void delete(Provider provider);
    public List<Provider> findAll();
    public Provider findbyProviderId(int idProvider);
     public Provider findByProviderCountry(String ProviderCountry);
}
