/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.ProductDao;
import com.pnv.dao.ProviderDao;
import com.pnv.models.Product;
import com.pnv.models.Provider;
import com.pnv.utils.Constant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author thangnd
 */
@Controller
@RequestMapping(value = "api/product")
public class ProductApiController {

    @Autowired
    private ProductDao productDao;

    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Product> viewProductPage() {
        /**
         * Get all titles
         */
        List<Product> product_list = productDao.findAll();
        return product_list;
    }
    @RequestMapping(value = "{idProduct}", method = RequestMethod.GET)
    public  @ResponseBody Product getProductByID(@PathVariable(value = "idProduct") Integer idProduct) {
        
//        Product emp = new Product();
//        emp.setImage("ddd");
//        emp.setName("ssss");
//        emp.setPrice(12525);
//        emp.setQuantity(2);
//        
//        List<Product> emp_lst = new ArrayList<Product>();
        Product pvr = productDao.findByProductId(idProduct);
       // dep.setEmployees(emp_lst);
        return productDao.findByProductId(idProduct);
    }
    
     @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Product productForm = new Product();
        map.addAttribute("productForm", productForm);
        return "product-add";

    }
     @RequestMapping(value = "/add/{image}/{name}/{price}/{quantity}", method = RequestMethod.GET)
    public @ResponseBody  List<Product> AddNewBefore(@PathVariable(value = "image") String image,
                               @PathVariable(value = "name") String name,
                               @PathVariable(value = "price") float price,
                               @PathVariable(value = "quantity") int quantity,
                              // @PathVariable(value = "productionDate") Date productionDate,
                              // @PathVariable(value = "expireDate") Date expireDate,
                                ModelMap map) {

        //Normally the parameter would be used to retrieve the object
        //In this case we keep it simple and return the name
         Product st = new Product();
        st.setImage(image);
       st.setName(name);
       st.setPrice(price);
       st.setQuantity(quantity);
    //   st.setProductionDate(productionDate);
     //  st.setExpireDate(expireDate);
        map.put("productObject", st);
        productDao.saveOrUpdate(st);
        List<Product> product_list = productDao.findAll();        
        return product_list;
    }
     //xoa
        @RequestMapping(value = "/delete/{idProduct}", method = RequestMethod.GET)
    public String doDeleteProduct(@PathVariable(value = "idProduct") int idProduct, ModelMap map) {

        productDao.delete(productDao.findByProductId(idProduct));

        return Constant.REDIRECT + "/product";

    }
        //sua
        @RequestMapping(value = "/edit/{idProduct}/{name}/{price}/{quantity}", method = RequestMethod.GET)
    public String viewEditStudentPage(@PathVariable(value = "idProduct") int idProduct,  
                               // @PathVariable(value = "image") String image,
                              @PathVariable(value = "name") String name,
                              @PathVariable(value = "price") float price,
                              @PathVariable(value = "quantity") int quantity,
                            //  @PathVariable(value = "productionDate") Date productionDate,
                             //  @PathVariable(value = "expireDate") Date expireDate,
                                    ModelMap map) {

        Product pdEdit = productDao.findByProductId(idProduct);
       // pdEdit.setImage(image);
        pdEdit.setName(name);
        pdEdit.setPrice(price);
        pdEdit.setQuantity(quantity);
       // pdEdit.setProductionDate(productionDate);
       // pdEdit.setExpireDate(expireDate);
        productDao.saveOrUpdate(pdEdit);
//        map.addAttribute("studentEdit", studentEdit);
//        return "resultEdit";
        return Constant.REDIRECT + "/product";
    }
}
