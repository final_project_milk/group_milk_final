/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.ProviderDao;
import com.pnv.models.Product;
import com.pnv.models.Provider;
import com.pnv.utils.Constant;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author thangnd
 */
@Controller
@RequestMapping(value = "api/provider")
public class ProviderApiController {
    @Autowired
    private ProviderDao providerDao;

    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Provider> viewProviderPage() {
        /**
         * Get all titles
         */
        List<Provider> provider_list = providerDao.findAll();
        return provider_list;
    }
    @RequestMapping(value = "{idProvider}", method = RequestMethod.GET)
    public  @ResponseBody Provider getProviderByID(@PathVariable(value = "idProvider") Integer idProvider) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Provider pvr = providerDao.findbyProviderId(idProvider);
       // dep.setEmployees(emp_lst);
        return providerDao.findbyProviderId(idProvider);
    }
    
     @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Provider providerForm = new Provider();
        map.addAttribute("providerForm", providerForm);
        return "provider-add";

    }
     @RequestMapping(value = "/add/{nameOfCountry}", method = RequestMethod.GET)
    public @ResponseBody  List<Provider> AddNewBefore(@PathVariable(value = "nameOfCountry") String nameOfCountry,
                               
                                ModelMap map) {

        //Normally the parameter would be used to retrieve the object
        //In this case we keep it simple and return the name
         Provider st = new Provider();
        st.setNameOfCountry(nameOfCountry);
       
        map.put("providerObject", st);
        providerDao.saveOrUpdate(st);
        List<Provider> provider_list = providerDao.findAll();        
        return provider_list;
    }
     //xoa
        @RequestMapping(value = "/delete/{idProvider}", method = RequestMethod.GET)
    public String doDeleteTitle(@PathVariable(value = "idProvider") int idProvider, ModelMap map) {

        providerDao.delete(providerDao.findbyProviderId(idProvider));

        return Constant.REDIRECT + "/provider";

    }
        //sua
        @RequestMapping(value = "/edit/{idProvider}/{nameOfCountry}", method = RequestMethod.GET)
    public String viewEditStudentPage(@PathVariable(value = "idProvider") int idProvider,  
                                @PathVariable(value = "nameOfCountry") String nameOfCountry,
                              
                                    ModelMap map) {

        Provider pdEdit = providerDao.findbyProviderId(idProvider);
        pdEdit.setNameOfCountry(nameOfCountry);
        
        providerDao.saveOrUpdate(pdEdit);
//        map.addAttribute("studentEdit", studentEdit);
//        return "resultEdit";
        return Constant.REDIRECT + "/provider";
    }
}
