/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.TypeproductDao;
import com.pnv.models.Provider;
import com.pnv.models.Typeproduct;
import com.pnv.utils.Constant;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author thangnd
 */
@Controller
@RequestMapping(value = "api/typeproduct")
public class TypeproductApiController {
    @Autowired
    private TypeproductDao typeproductDao;
    public TypeproductApiController() {}
    @RequestMapping(method = RequestMethod.GET )
    public @ResponseBody    List<Typeproduct> getTypeproduct() {

        /**
         * Get all titles
         */ 
        List<Typeproduct> typeproduct_list = typeproductDao.findAll();
        
        return typeproduct_list;
    }
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Typeproduct typeproductForm = new Typeproduct();
        map.addAttribute("typeproductForm", typeproductForm);
        return "typeproduct-add";

    }
    @RequestMapping(value = "/add/{typeProduct}", method = RequestMethod.GET)
    public @ResponseBody  List<Typeproduct> AddNewBefore(@PathVariable(value = "typeProduct") String typeProduct,
                               
                                ModelMap map) {

        //Normally the parameter would be used to retrieve the object
        //In this case we keep it simple and return the name
         Typeproduct st = new Typeproduct();
        st.setTypeProduct(typeProduct);
       
        map.put("typeproductObject", st);
        typeproductDao.saveOrUpdate(st);
        List<Typeproduct> typeproduct_list = typeproductDao.findAll();        
        return typeproduct_list;
    }
    //xoa
        @RequestMapping(value = "/delete/{idTypeProduct}", method = RequestMethod.GET)
    public String doDeleteTitle(@PathVariable(value = "idTypeProduct") int idTypeProduct, ModelMap map) {

        typeproductDao.delete(typeproductDao.findByTypeproductId(idTypeProduct));

        return Constant.REDIRECT + "/typeproduct";

    }
         //sua
        @RequestMapping(value = "/edit/{idTypeProduct}/{typeProduct}", method = RequestMethod.GET)
    public String viewEditStudentPage(@PathVariable(value = "idTypeProduct") int idTypeProduct,  
                                @PathVariable(value = "typeProduct") String typeProduct,
                              
                                    ModelMap map) {

        Typeproduct pdEdit = typeproductDao.findByTypeproductId(idTypeProduct);
        pdEdit.setTypeProduct(typeProduct);
        
        typeproductDao.saveOrUpdate(pdEdit);
//        map.addAttribute("studentEdit", studentEdit);
//        return "resultEdit";
        return Constant.REDIRECT + "/typeproduct";
    }
}
