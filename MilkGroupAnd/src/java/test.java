
import com.pnv.models.Product;
import com.pnv.models.Provider;
import com.pnv.utils.HibernateUtil;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author thangnd
 */
public class test {

    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Provider> provider = session.createQuery("from provider ").list();
        for (Iterator<Provider> iterator = provider.iterator(); iterator.hasNext();) {
            Provider next = iterator.next();
            if (next.getProducts().size() > 0) {
                System.out.println(next.getNameOfCountry() + "----" + next.getProducts().get(0).getName());
            }
        }
        session.close();
    }
}
